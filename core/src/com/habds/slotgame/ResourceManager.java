package com.habds.slotgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class ResourceManager {
    private static ResourceManager instance;
    public BitmapFont font;
    public TextureAtlas buttonsAtlas; //** image of buttons **//
    public Skin buttonSkin, uiSkin; //** images are used as skins of the button **//

    public ResourceManager() {
        load();
        instance = this;
    }

    public static ResourceManager getInstance() {
        if (instance == null) instance = new ResourceManager();
        return instance;
    }

    private void load() {
        buttonsAtlas = new TextureAtlas("testResources/button.pack"); //**button atlas image **//
        buttonSkin = new Skin();
        buttonSkin.addRegions(buttonsAtlas); //** skins for on and off **//
        uiSkin = new Skin(Gdx.files.internal("testResources/uiskin.json"));
        font = new BitmapFont(Gdx.files.internal("testResources/default.fnt"), false); //** font **//
    }
}
