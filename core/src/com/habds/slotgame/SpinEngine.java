package com.habds.slotgame;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.habds.slotgame.reels.Reel;
import com.habds.slotgame.symbol.Symbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SpinEngine { // class isn`t used anywhere!
    private class ReelState {
        int viewAreaPos, reelMaxLength;
        final double reelPartSize;
        // private int firstVisibleSymbol;
        // private int lastVisibleSymbol;
        List<Symbol> symbolsToDraw;

        public ReelState(Reel reel) {
            reelPartSize = reel.getSymbolHeight();
            reelMaxLength = reel.getChildren().size() * (int) (reelPartSize);
            viewAreaPos = (int) (reelMaxLength - 3 * reelPartSize);
        }
    }

    float srcY;
    //   Reel reel;
    //   ReelState curReelState;
    Map<Reel, ReelState> reelsMap;
    Batch batch;

    public SpinEngine() {
        reelsMap = new HashMap<>();
    }

    public SpinEngine(Batch batch) {
        this();
        this.batch = batch;
    }

    public void registerReel(Reel reel) {
        ReelState reelState = new ReelState(reel);
        reelsMap.put(reel, reelState);
        update(reel);
        //  reel.setSpinEngine(this);

    }


    /*public void spinAll(){
        for(Reel reel:reelsMap.keySet()) reel.setSpinning(true);
    }

    public void spin(Reel reel) {
        draw(reel, reelsMap.get(reel),reel.isSpinning());
    }
    */

    public void draw(Reel reel, ReelState curReelState, boolean isSpinning) {
        if (isSpinning)
            update(reel);
        for (int i = 0; i < curReelState.symbolsToDraw.size(); i++) {
            Symbol symbol = curReelState.symbolsToDraw.get(i);
            symbol.update();
            double k = (double) symbol.getRegionHeight() / reel.getSymHeight(); // відношення текселів до пікслеів
            if (i == 0)
                batch.draw(symbol.getTexture(),              //висота з якої починає малюватис спрайт
                        symbol.getX(),
                        symbol.getY(),
                        reel.getSymWidth() / 2,
                        reel.getSymHeight() / 2,
                        reel.getSymWidth(),
                        reel.getSymHeight() - srcY,                 //якої висоти робити спрайт
                        symbol.getScaleX(),
                        symbol.getScaleY(),
                        0,
                        symbol.getRegionX(),
                        (int) (srcY * k),                 //початкова позиція на текстурі для малювання
                        symbol.getRegionWidth(),
                        (int) (symbol.getRegionHeight() - srcY * k),       //скільки текстури малювати
                        false,
                        false);
            else if (i == curReelState.symbolsToDraw.size() - 1) {
                batch.draw(symbol.getTexture(),
                        symbol.getX(),
                        symbol.getY() + (reel.getSymHeight() - srcY),
                        reel.getSymWidth() / 2,
                        reel.getSymHeight() / 2,
                        reel.getSymWidth(),
                        srcY,
                        symbol.getScaleX(),
                        symbol.getScaleY(),
                        0,
                        symbol.getRegionX(),
                        symbol.getRegionY(),
                        symbol.getRegionWidth(),
                        (int) (srcY * k),
                        false,
                        false);
            } else symbol.draw(batch);

        }
    }


    public void update(Reel reel) {
        ReelState reelState = reelsMap.get(reel);
        reelState.viewAreaPos -= 1;
        if (reelState.viewAreaPos < 0) reelState.viewAreaPos = reelState.reelMaxLength - 1;
        srcY = (float) (reelState.viewAreaPos % reelState.reelPartSize);
        reelState.symbolsToDraw = findSymbolsToDraw(reel);       //постав брейкпоінт з умовою : (reelMaxLength-viewAreaPos)%reelPartSize==0
        for (int i = 0; i < reelState.symbolsToDraw.size(); i++) {
            Symbol symbol = reelState.symbolsToDraw.get(i);
            symbol.setOffsetY((int) ((reel.getHeight() - reelState.reelPartSize) - (i * reelState.reelPartSize) + srcY));
        }
        reelsMap.put(reel, reelState);
    }

    /*
    * Цей метод робить масив символів які будуть малюватись (видимі)
    * При цьому він на кожному кроці цей масив очищає і формує заново.
    * Є інший варіант. Віддати роботу промальовки напряму символу, але тоді
    * йому треба буде знати його місце в масиві і таку змінну як srcY,
    * шо, я думаю, йому знати не треба. Чи не сильно страшно для "проізводітєльності" ця очистка масиву на кожному кадрі?
    * */
    private List<Symbol> findSymbolsToDraw(Reel reel) {
        ReelState curReelState = reelsMap.get(reel);
        List<Symbol> symbolList = new ArrayList<>();
        int firstVisibleSymbol = (int) (curReelState.viewAreaPos / curReelState.reelPartSize);
        int lastVisibleSymbol = (int) ((curReelState.viewAreaPos + 3 * curReelState.reelPartSize) % curReelState.reelMaxLength / curReelState.reelPartSize);
        if (firstVisibleSymbol < lastVisibleSymbol)
            for (int i = firstVisibleSymbol; i <= lastVisibleSymbol; i++)
                symbolList.add(reel.getChildren().get(i));
        else {
            for (int i = firstVisibleSymbol; i != lastVisibleSymbol; i++) {
                symbolList.add(reel.getChildren().get(i));
                if (i >= reel.getChildren().size() - 1) i = -1;
            }
            symbolList.add(reel.getChildren().get(lastVisibleSymbol));  // останній елемент в циклі не додастся
        }
        return symbolList;
    }

}
