package com.habds.slotgame.screens;

import aurelienribon.tweenengine.TweenManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.habds.slotgame.ResourceManager;
import com.habds.slotgame.elements.CountUp;
import com.habds.slotgame.items.BetSelector;
import com.habds.slotgame.reels.Container;
import com.habds.slotgame.reelset.Reelset;
import com.habds.slotgame.reelset.ReelsetGenerator;
import com.habds.slotgame.reelset.ReelsetLoader;
import com.habds.slotgame.reelset.Result;
import com.habds.slotgame.utils.IReelsetGenerator;
import com.habds.slotgame.utils.Observer;
import com.habds.slotgame.utils.Renderable;

import java.util.ArrayList;
import java.util.List;

public class SlotScreen implements Screen, Observer {
    SpriteBatch batch;
    private Camera camera;
    List<Renderable> items = new ArrayList<Renderable>();
    Stage stage;
    public TextButton button;

    IReelsetGenerator generator, loader;
    Result result;
    Reelset reelset;

    private static TweenManager tweenManager;
    CountUp countUpLabel;
    BetSelector betSelector;

    public static TweenManager getTweenManager() {
        return tweenManager;
    }

    @Override
    public void show() {
        ResourceManager resourceManager = ResourceManager.getInstance();
        batch = new SpriteBatch();
        stage = new Stage();
        initializeOtherElements();

        result = loader.generateResult(); //ініціалізація  рілсету
        reelset.initialize(result.getReelSetModel());
        reelset.addObserver(this);

        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle(); //** Button properties **//
        style.up = resourceManager.buttonSkin.getDrawable("buttonOff");
        style.down = resourceManager.buttonSkin.getDrawable("buttonOn");
        style.font = resourceManager.font;

        button = new TextButton("", style);
        //** Button text and style **//
        button.setWidth(Gdx.graphics.getWidth() / 4);
        button.setHeight(Gdx.graphics.getHeight() / 3);


        button.setPosition(10, 10);
        button.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!reelset.isSpinning()) {
                    reelset.spinReelset();
                } else reelset.stopReelset();
                return true;
            }
        });

        countUpLabel = new CountUp("0", resourceManager.uiSkin);
        countUpLabel.setPosition(500, 100);

        betSelector = new BetSelector(resourceManager.uiSkin);
        betSelector.setPosition(400, 100);
        betSelector.setSize(200, 80);

        items.add(reelset);

        stage.addActor(betSelector);
        stage.addActor(button);
        stage.addActor(countUpLabel);
        camera = new OrthographicCamera(1280f, 720f);
        camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
        camera.update();

        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        Gdx.gl.glClearColor(1, 1, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (reelset.isVisibleBorders) for (Container reel : reelset.getChildren()) reel.drawBorder();   //for debugging
        stage.draw();

        batch.begin();
        for (Renderable item : items) {
            item.update();
            item.draw(batch);
        }
        tweenManager.update(Gdx.graphics.getDeltaTime());
        batch.end();
    }

    public CountUp getCountUpLabel() {
        return countUpLabel;
    }

    private void initializeOtherElements() {
        generator = new ReelsetGenerator();
        loader = new ReelsetLoader();
        reelset = new Reelset(this);
        tweenManager = new TweenManager();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void update(Result result) {
        countUpLabel.showNewValue(result.getCoins());
    }
}
