package com.habds.slotgame.tweenEngine;

import aurelienribon.tweenengine.TweenAccessor;
import com.habds.slotgame.elements.CountUp;

public class CountUpAccesor implements TweenAccessor<CountUp> {

    public final static int COINS = 0;

    @Override
    public int getValues(CountUp target, int tweenType, float[] returnValues) {

        switch (tweenType) {
            case COINS:
                returnValues[0] = target.getCoins();
                return 1;
            default:
                assert false;
                return 0;
        }
    }

    @Override
    public void setValues(CountUp target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case COINS:
                target.setCoins((int) newValues[0]);
                break;

            default:
                assert false;
                break;
        }
    }
}
