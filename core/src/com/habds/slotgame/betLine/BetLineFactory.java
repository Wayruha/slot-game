package com.habds.slotgame.betLine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.habds.slotgame.symbol.Symbol;

import java.util.List;

public class BetLineFactory {
    int width = 620, height = 300;

    public BetLine createBetLine(List<? extends Symbol> points) {
        Color color = new Color(150, 0, 0, 1);
        BetLine betLine = new BetLine(points, color);
        betLine.setTexture(createTexture(betLine));
        return betLine;
    }

    private Texture createTexture(BetLine betLine) {
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        Color color = betLine.getColor() != null ? betLine.getColor() : new Color(1, 1, 1, 1);
        List<? extends Symbol> points = betLine.getPoints();
        pixmap.setColor(color);
        if (points != null) {
            for (int i = 0; i < points.size() - 1; i++) {
                Symbol from = points.get(i);
                Symbol to = points.get(i + 1);
                int fromX = (int) (from.getXRelativeToReelset() + from.getWidth() / 2);
                int fromY = height - (int) (from.getYRelativeToReelset() + from.getHeight() / 2);
                int toX = (int) (to.getXRelativeToReelset() + to.getWidth() / 2);
                int toY = height - (int) (to.getYRelativeToReelset() + to.getHeight() / 2);
                pixmap.drawLine(fromX, fromY, toX, toY);
            }
        }

        Texture texture = new Texture(pixmap);
        pixmap.dispose();
        return texture;
    }

    public BetLineFactory() {
    }
}
