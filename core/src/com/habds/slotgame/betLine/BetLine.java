package com.habds.slotgame.betLine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.habds.slotgame.symbol.Symbol;

import java.util.List;

public class BetLine {
    Texture texture;
    List<? extends Symbol> points;
    Color color;

    public BetLine(List<? extends Symbol> points, Color color) {
        this.points = points;
        this.color = color;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public List<? extends Symbol> getPoints() {
        return points;
    }

    public void setPoints(List<? extends Symbol> points) {
        this.points = points;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
