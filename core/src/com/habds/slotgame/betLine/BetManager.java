package com.habds.slotgame.betLine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.habds.slotgame.reelset.Result;
import com.habds.slotgame.symbol.Symbol;
import com.habds.slotgame.utils.Observer;

import java.util.ArrayList;
import java.util.List;

public class BetManager implements Observer {
    static final int[][] betLinesMap = {{1, 1, 1, 1, 1}, {0, 0, 0, 0, 0}, {2, 2, 2, 2, 2}, {0, 1, 2, 1, 0},
            {2, 1, 0, 1, 2}, {0, 0, 1, 0, 0}, {2, 2, 1, 2, 2}, {1, 2, 2, 2, 1}, {2, 1, 1, 1, 2}};
    ArrayList<BetLine> betLines, activeLines;
    BetLineFactory factory;
    double timer;
    final double deltaBetweenShows = 1.5;
    int activeLineIndex;

    public void loadBetLines(List<List<Symbol>> initReelset) {
        for (int[] line : betLinesMap) {
            List<Symbol> points = new ArrayList<>();
            for (int i = 0; i < line.length; i++) {
                points.add(initReelset.get(i).get(line[i]));
            }
            betLines.add(factory.createBetLine(points));
        }
    }

    public BetManager(List<List<Symbol>> initReelset) {
        betLines = new ArrayList<>();
        activeLines = new ArrayList<>();
        factory = new BetLineFactory();
        loadBetLines(initReelset);
        timer = deltaBetweenShows;
        activeLineIndex = 0;
    }

    private void showLines(List<Integer> winLines) {
        activeLines.clear();
        for (int winLine : winLines) {
            activeLines.add(betLines.get(winLine - 1));
        }
    }

    public ArrayList<Texture> getTexturesToDraw() {
        ArrayList<Texture> textures = new ArrayList<>();
        if (activeLines.size() > 0) {
            if (timer < 0) {
                activeLineIndex = (activeLineIndex + 1) % activeLines.size();
                timer = deltaBetweenShows;
            }
            textures.add(activeLines.get(activeLineIndex).getTexture());
            timer -= Gdx.graphics.getDeltaTime();
        }
        return textures;
    }

    @Override
    public void update(Result arg) {           //TODO
        showLines(arg.getBetlines());
    }
}
