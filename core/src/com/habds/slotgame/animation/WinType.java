package com.habds.slotgame.animation;

public enum WinType {
    FREE_SPINS,
    WIN,
    NONE,
}
