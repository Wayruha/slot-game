package com.habds.slotgame.utils;

import com.habds.slotgame.reelset.Result;

public interface Observer {
    public void update(Result Result);
}
