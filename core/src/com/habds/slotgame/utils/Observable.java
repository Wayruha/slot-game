package com.habds.slotgame.utils;

import java.util.ArrayList;

public interface Observable {
    ArrayList<Observer> obsList = new ArrayList<>();

    default public void addObserver(Observer obs) {
        obsList.add(obs);
    }

    default public void removeObserver(Observer obs) {
        obsList.remove(obs);
    }

    void notifyObservers();
}
