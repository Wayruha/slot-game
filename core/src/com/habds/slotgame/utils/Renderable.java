package com.habds.slotgame.utils;

import com.badlogic.gdx.graphics.g2d.Batch;

public interface Renderable {
    void draw(Batch batch);

    void update();
}
