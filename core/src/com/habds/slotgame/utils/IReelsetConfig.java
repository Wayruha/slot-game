package com.habds.slotgame.utils;

public interface IReelsetConfig {
    public int getReelWidth();
    public int getReelHeight();
    public int getSymWidth();
    public int getSymHeight();
    public double getVerticalSpacingBtwSymbols();
    public double getHorizontalSpacingBtwSymbols();
    public int getSymbolsCountInReel();
    public int getSpacingBetweenReels();
}
