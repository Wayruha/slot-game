package com.habds.slotgame.utils;

import com.habds.slotgame.reelset.Result;

public interface IReelsetGenerator {

    public Result generateResult();

}
