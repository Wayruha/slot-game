package com.habds.slotgame.reels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.habds.slotgame.reelset.Reelset;
import com.habds.slotgame.reelset.ReelsetConfig;
import com.habds.slotgame.symbol.Symbol;
import com.habds.slotgame.symbol.SymbolType;

import java.util.ArrayList;
import java.util.List;

//ArrayList<Symbol> children --  in super class
// addChild() - parent`s method
public class Reel extends Container<Symbol> {
    final int symWidth;               //розміри символа
    final int symHeight;
    final double hSpacingBtwSymbols;
    final double vSpacingBtwSymbols;
    final double symbolHeight;
    int localPositionY, reelMaxLength;
    double spinTime;
    float srcY;
    SpinningType spinningType;
    List<Symbol> symbolsToDraw;
    ReelsetConfig config;
    ArrayList<Symbol> preparedState, replaced = new ArrayList<>();

    private int visibleSymbolsCount = 3;
    int replacedSymbolsPos;
    private boolean isExtraSpin = false;
    double currentSpinTime;

    private ArrayList<PostponedAction> waitList = new ArrayList<>();

    private class PostponedAction {
        Runnable runnable;
        int waitingSymbols;

        public void symbolGone() {
            waitingSymbols--;
            if (waitingSymbols == 0) {
                runnable.run();
                waitList.remove(this);
            }
        }

        public PostponedAction(Runnable runnable, int waitingSymbols) {
            this.runnable = runnable;
            this.waitingSymbols = waitingSymbols;
        }
    }

    //TODO подумати над тим що б зробити вкладений клас для localPosition який буде містити значення змінної і сам обрізати по верху і по низу.

    public Reel(Container parent, ReelsetConfig config, int x, int y) {
        super(x, y, config.getReelWidth(), config.getReelHeight());
        this.parent = parent;
        this.config = config;

        this.symHeight = config.getSymHeight();
        this.symWidth = config.getSymWidth();
        this.hSpacingBtwSymbols = config.getHorizontalSpacingBtwSymbols();
        this.vSpacingBtwSymbols = config.getVerticalSpacingBtwSymbols();
        symbolHeight = symHeight;//+vSpacingBtwSymbols;
    }

    public void initialize(List<SymbolType> list) {
        newChildren(createSymbols(list));
        initialize();
    }

    private ArrayList<Symbol> createSymbols(List<SymbolType> list) {
        ArrayList<Symbol> symbols = new ArrayList<>();
        for (SymbolType symType : list) {
            Symbol symbol = new Symbol(symType, this);
            symbol.setSize(symWidth, symHeight);
            symbol.setOffsetX((int) hSpacingBtwSymbols);
            symbols.add(symbol);
        }
        return symbols;
    }


    private void initialize() {
        reelMaxLength = children.size() * (int) (symbolHeight);
        localPositionY = convertToLocalPos(reelMaxLength - 3 * symbolHeight);
        updateVisibleSymbols();
        spinningType = SpinningType.none;
    }


    public void draw(Batch batch) {
        batch.end();
        batch.begin();
        for (int i = 0; i < symbolsToDraw.size(); i++) {
            Symbol symbol = symbolsToDraw.get(i);
            symbol.draw(batch, i);
        }
    }

    public void checkSpinning() {
        if (isOnBorderOfSymbols()) {
            symbolGone();
        }
        if (!isExtraSpin) {
            if (currentSpinTime < spinTime * spinningType.getTimeInPercent()) {
                currentSpinTime += Gdx.graphics.getDeltaTime();
            } else {
                if (isExtraSpinNeeded())   //крутити далі якщо це не сповільнення, або символи не на своїх місцях
                    return;
                else
                    nextSpinState();
            }
        }

    }

    private void waitAndDo(int i, Runnable runnable) {
        PostponedAction action = new PostponedAction(runnable, i);
        waitList.add(action);
    }

    private void symbolGone() {
        for (int i = 0; i < waitList.size(); i++) {
            PostponedAction action = waitList.get(i);
            action.symbolGone();
        }
    }

    private boolean isExtraSpinNeeded() {
        if (spinningType == SpinningType.slowDown) {
            if (isOnBorderOfSymbols()) {
                makeExtraSpin();
            }
            return true;    //якшо воно лежить в межах похибки то вже можна робити докрутку
        }
        return false;
    }

    private boolean isOnBorderOfSymbols() {
        return srcY <= spinningType.getSpeed() / 2 || srcY >= symbolHeight - spinningType.getSpeed() / 2;
    }

    private void makeExtraSpin() {
        isExtraSpin = true;
        if (srcY > symbolHeight / 2) {
            localPositionY = convertToLocalPos(localPositionY + symbolHeight - srcY);
        } //якщо src~99 то треба прокрутити трохи назад шо б він став , бо буде баг
        waitAndDo(visibleSymbolsCount, () -> {
            nextSpinState();
            reelStopped();
        });
        replaceSymbols();
        // System.out.println("START slowdown" + this.toString()+ ". Position to stop=" + positionToStop +";srcY="+srcY+". Elements:"+symbolsToDraw);
    }

    private void nextSpinState() {
        currentSpinTime = 0;
        isExtraSpin = false;
        spinningType = spinningType.next();
    }

    private void reelStopped() {
        // System.out.println("Reel " + this.toString() + " has stopped; srcY=" + srcY +". Elements:+"+symbolsToDraw);
        ((Reelset) parent).countDown();
        if (isOnBorderOfSymbols()) {        //виправляємо косметичний дефект, коли видно частину зайвого символу
            srcY = srcY < symbolHeight / 2 ? 0 : 99;
        }
    }

    private void replaceSymbols() {
        int size = children.size();
        int firstVisibleSymbol = (int) (localPositionY / symbolHeight);
        replaced.add(children.set((firstVisibleSymbol - 1 + size) % size, preparedState.get(0)));
        replaced.add(children.set((firstVisibleSymbol - 2 + size) % size, preparedState.get(1)));
        replaced.add(children.set((firstVisibleSymbol - 3 + size) % size, preparedState.get(2)));
        replacedSymbolsPos = projectionOnChildrenSize(firstVisibleSymbol - visibleSymbolsCount);
    }

    private void restoreSymbols() {
        for (int i = 0; i < replaced.size(); i++) {
            children.set(replacedSymbolsPos, replaced.get(visibleSymbolsCount - 1 - i));                  //TODO погратись з крутінням і остановокю і найдеш баг
        }
        replaced.clear();
    }

    public void update() {
        if (spinningType != SpinningType.none) {
            checkSpinning();
            if (isSpinning()) {
                localPositionY = convertToLocalPos(localPositionY - spinningType.getSpeed());
                srcY = (float) (localPositionY % symbolHeight);
                updateVisibleSymbols();
            }
        }
        super.update();
    }

    private void updateVisibleSymbols() {
        symbolsToDraw = findSymbolsToDraw();       //постав брейкпоінт з умовою : (reelMaxLength-localPositionY)%symbolHeight==0
        for (int i = 0; i < symbolsToDraw.size(); i++) {
            Symbol symbol = symbolsToDraw.get(i);
            symbol.setOffsetY((int) ((getHeight() - symbolHeight) - (i * symbolHeight) + srcY));
        }
    }

    private List<Symbol> findSymbolsToDraw() {
        List<Symbol> symbolList = new ArrayList<>();
        int firstVisibleSymbol = (int) (localPositionY / symbolHeight);
        int lastVisibleSymbol = ((int) (convertToLocalPos(localPositionY + 3 * symbolHeight) / symbolHeight));
        if (firstVisibleSymbol < lastVisibleSymbol)
            for (int i = firstVisibleSymbol; i <= lastVisibleSymbol; i++)
                symbolList.add(children.get(i));
        else {
            for (int i = firstVisibleSymbol; i != lastVisibleSymbol; i++) {
                symbolList.add(children.get(i));
                if (i >= children.size() - 1) i = -1;
            }
            symbolList.add(children.get(lastVisibleSymbol));  // останній елемент в циклі не додастся
        }
        return symbolList;
    }

    public void spin(List<SymbolType> preparedState) {
        this.preparedState = createSymbols(preparedState);

        currentSpinTime = 0;
        spinningType = SpinningType.speedUp;
//        waitAndDo(visibleSymbolsCount, () -> restoreSymbols());
        waitAndDo(visibleSymbolsCount, new Runnable() {
            @Override
            public void run() {
                restoreSymbols();
            }
        });
    }

    public void stopSpinning() {
        if (spinningType != SpinningType.none) {
            spinningType = SpinningType.slowDown;
        }
    }

    private int convertToLocalPos(int pos) {     //проектуємо значення в проміжок {0,reelMaxLength}
        return (pos + reelMaxLength) % reelMaxLength;
    }

    private int convertToLocalPos(double pos) {     //проектуємо значення в проміжок {0,reelMaxLength}
        return (int) ((pos + reelMaxLength) % reelMaxLength);
    }

    private int projectionOnChildrenSize(int index) {
        return (index + children.size()) % children.size();
    }

    public boolean isSpinning() {
        return spinningType != SpinningType.none;
    }

    public void setSpinTime(double spinTime) {
        this.spinTime = spinTime;
    }

    public float getSrcY() {
        return srcY;
    }

    public List<Symbol> getVisibleSymbols() {
        return symbolsToDraw;
    }

    //варіант з спінЕджіном

    public int getSymWidth() {
        return symWidth;
    }

    public int getSymHeight() {
        return symHeight;
    }

    public double getSymbolHeight() {
        return symbolHeight;
    }

    /* public void setSpinEngine(SpinEngine engine) {
        this.spinEngine = engine;
    }*/

    @Override
    public void addChild(Symbol symbol) {
        super.addChild(symbol);
        reelMaxLength = children.size() * (int) (symbolHeight);//костильно виправляємо баг, що інколи, коли позиція = макс позиції то йде попитка взяти символ якого не існує
    }

    @Override
    public void addChild(int index, Symbol symbol) {
        super.addChild(index, symbol);
        reelMaxLength = children.size() * (int) (symbolHeight);
    }

    @Override
    public void removeChild(int index) {
        super.removeChild(index);
        reelMaxLength = children.size() * (int) (symbolHeight);
    }

    @Override
    public String toString() {
        return "Reel X=" + x + "; pos:" + localPositionY + ";";
    }
}

