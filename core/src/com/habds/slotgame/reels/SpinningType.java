package com.habds.slotgame.reels;

public enum SpinningType {
    speedUp(10, 0.2),
    normal(7, 0.7),
    slowDown(3, 0.1),
    none(0);

    private int speed;
    private double timeInPercent;

    SpinningType(int speed, double timeInPercent) {
        this.speed = speed;
        this.timeInPercent = timeInPercent;
    }

    SpinningType(int speed) {
        this.speed = speed;
    }

    public SpinningType next() {
        if (this == none) return this;
        return SpinningType.values()[this.ordinal() + 1];
    }


    public int getSpeed() {
        return this.speed;
    }

    public double getTimeInPercent() {
        if (timeInPercent < 0) timeInPercent = 0;
        return this.timeInPercent;
    }
}
