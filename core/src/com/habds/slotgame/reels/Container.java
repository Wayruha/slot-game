package com.habds.slotgame.reels;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.habds.slotgame.utils.Renderable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Container<T extends Renderable> implements Renderable {
    protected List<T> children = new ArrayList<>();
    int x;
    int y;
    Container parent;
    int width;
    int height;
    public boolean visibleBorders = true;
    private ShapeRenderer renderer;   // only for debug

    public Container(T... el) {
        Collections.addAll(children, el);
    }

    public Container(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        renderer = new ShapeRenderer();

    }

    public Container() {
    }

    public void addChild(T el) {
        children.add(el);
    }

    public void addChild(int index, T el) {
        children.add(index, el);
    }

    public void addChildren(List<T> children) {
        for (T child : children) {
            addChild(child);
        }
    }

    public void addChildren(int index, List<T> children) {
        for (T child : children) {
            addChild(index++, child);
        }
    }

    public void newChildren(List<T> children) {    //не можемо кастувати масив обєктів в інший масив
        this.children.clear();
        addChildren(children);
    }

    public void removeChild(int index) {
        this.children.remove(index);
    }

    public void removeChild(T obj) {
        this.children.remove(this.children.indexOf(obj));
    }

    public void drawBorder() {
        if (visibleBorders) {
            renderer.begin(ShapeRenderer.ShapeType.Line);
            renderer.setColor(new Color(170, 0, 0, 1));
            renderer.rect(getX(), getY(), getWidth(), getHeight());
            renderer.end();
        }
    }

    public List<T> getChildren() {
        return children;
    }

    public int getX() {
        return x + (parent == null ? 0 : parent.getX());
    }            //TODO Вертає абсолютну позицію, а сеттер ставить відносну

    public int getOffsetX() {
        return this.x;
    }

    public void setOffsetX(int x) {
        this.x = x;
    }

    public int getY() {
        return y + (parent == null ? 0 : parent.getY());
    }

    public int getOffsetY() {
        return this.y;
    }

    public void setOffsetY(int y) {
        this.y = y;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }


    @Override
    public void draw(Batch batch) {
        for (T child : children) {
            child.draw(batch);
        }
    }

    @Override
    public void update() {
        for (T child : children) {
            child.update();
        }
    }
}
