package com.habds.slotgame.reelset;

import com.habds.slotgame.animation.WinType;

import java.util.List;

public class Result {
    private List<Integer> betlines; // виграшні бет лінії [1, 3, 10]
    private int coins; // 10
    private WinType winType; // enum {FREE_SPINS, BASIC} //small win, medium win, large win, big win - we should calculate by ourself on client side
    private ReelsetModel reelSetModel;

    public Result() {
    }

    public Result(List<Integer> betlines, int coins, ReelsetModel reelSetModel) {
        this.betlines = betlines;
        this.coins = coins;
        this.reelSetModel = reelSetModel;

        if (coins > 0) {
            winType = WinType.WIN;
        }
    }

    public WinType getWinType() {
        return winType;
    }

    public void setWinType(WinType winType) {
        this.winType = winType;
    }

    public List<Integer> getBetlines() {
        return betlines;
    }

    public void setBetlines(List<Integer> betlines) {
        this.betlines = betlines;
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public ReelsetModel getReelSetModel() {
        return reelSetModel;
    }

    public void setReelSetModel(ReelsetModel reelSetModel) {
        this.reelSetModel = reelSetModel;
    }

    @Override
    public String toString() {
        return "Result: lines " + betlines + "; coins: " + coins + "; model: " + reelSetModel;
    }
}
