package com.habds.slotgame.reelset;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.habds.slotgame.animation.WinType;
import com.habds.slotgame.betLine.BetManager;
import com.habds.slotgame.reels.Container;
import com.habds.slotgame.reels.Reel;
import com.habds.slotgame.screens.SlotScreen;
import com.habds.slotgame.symbol.Symbol;
import com.habds.slotgame.utils.IReelsetGenerator;
import com.habds.slotgame.utils.Observable;
import com.habds.slotgame.utils.Observer;
import com.habds.slotgame.utils.Renderable;

import java.util.ArrayList;
import java.util.List;

public class Reelset extends Container<Reel> implements Renderable, Observable {

    public boolean isVisibleBorders = true;
    boolean isSpinning = false;

    IReelsetGenerator generator;
    ReelsetConfig config;
    Result result;
    SlotScreen parent;

    WinType winType;
    int reelStopCountDown;
    BetManager betManager;


    public Reelset(SlotScreen parent) {
        this.parent = parent;
        generator = new ReelsetGenerator();
        config = new ReelsetConfig();
        setOffsetX(100);
        setOffsetY(200);
    }

    public void initialize(ReelsetModel model) {
        betManager = new BetManager(createReels(model));
        addObserver(betManager);
    }

    public void notifyObservers() {
        for (Observer obs : obsList) {
            obs.update(result);
        }
    }

    public void spinReelset() {
        preSpinning();
        for (int i = 0; i < getChildren().size(); i++) {
            Reel reel = getChildren().get(i);
            reel.spin(result.getReelSetModel().getReelset().get(i));
        }
    }

    @Override
    public void draw(Batch batch) {
        super.draw(batch);
        if (isWin() && isSpinning == false)
            for (Texture texture : betManager.getTexturesToDraw()) {
                batch.draw(texture, getX(), getY(), 620, 300);
            }
    }

    private void preSpinning() {
        System.out.println("Spin reelset!!");
        isSpinning = true;
        winType = WinType.NONE;
        reelStopCountDown = 5;
        parent.getCountUpLabel().reset();
        result = generator.generateResult();
    }

    private void postSpinning() {
        isSpinning = false;
        winType = result.getWinType();
        notifyObservers();
    }

    //тестовий метод
    private ArrayList<ArrayList<Symbol>> findWinSymbols() {
        ArrayList<ArrayList<Symbol>> winBetList = new ArrayList<>();
        ArrayList<Symbol> firstBetList = new ArrayList<>();
        firstBetList.add(children.get(0).getVisibleSymbols().get(0));
        firstBetList.add(children.get(1).getVisibleSymbols().get(1));
        firstBetList.add(children.get(2).getVisibleSymbols().get(1));
        winBetList.add(firstBetList);
        ArrayList<Symbol> secondBetList = new ArrayList<>();
        secondBetList.add(children.get(0).getVisibleSymbols().get(2));
        secondBetList.add(children.get(1).getVisibleSymbols().get(1));
        secondBetList.add(children.get(2).getVisibleSymbols().get(0));
        winBetList.add(secondBetList);
        return winBetList;
    }

    public void stopReelset() {
        for (Reel reel : getChildren()) {
            reel.stopSpinning();
        }
    }

    public List<List<Symbol>> createReels(ReelsetModel reelsetModel) {
        List<List<Symbol>> initReelsState = new ArrayList<>();
        getChildren().clear();
        for (int i = 0; i < 5; i++) {
            Reel reel = new Reel(this, config, i * (config.getReelWidth() + config.getSpacingBetweenReels()), 0);//(100 + i * (reelWidth + spacingBetweenReels), 200, reelWidth, reelHeight);
            reel.initialize(reelsetModel.getReelset().get(i));
            reel.setSpinTime(config.getReelsSpinTime()[i]);
            addChild(reel);
            initReelsState.add(reel.getVisibleSymbols().subList(0, 3));
        }
        return initReelsState;

    }

    public void countDown() {
        if (reelStopCountDown > 0) {
            this.reelStopCountDown--;
            if (reelStopCountDown == 0) postSpinning();
        } else
            System.out.println("There isn`t spinnig yet");
    }

    public boolean isSpinning() {
        return isSpinning;
    }

    public boolean isWin() {
        return winType == WinType.WIN;
    }

    private void saveReelsetInJson() {  //let it be here
     /* *//*   List<List<SymbolType>> confList = new ArrayList<>();
        confList.add(Arrays.asList(SymbolType.Scatter, SymbolType.Wild, SymbolType.Sym7));
        confList.add(Arrays.asList(SymbolType.Scatter, SymbolType.Sym7, SymbolType.Wild));
        confList.add(Arrays.asList(SymbolType.Wild, SymbolType.Sym7, SymbolType.Scatter));
        confList.add(Arrays.asList(SymbolType.Sym7, SymbolType.Scatter, SymbolType.Wild));
        confList.add(Arrays.asList(SymbolType.Scatter, SymbolType.Wild, SymbolType.Scatter));
*//*
        SymbolType[][] list = {{SymbolType.Scatter, SymbolType.Wild, SymbolType.Sym7},
                {SymbolType.Scatter, SymbolType.Sym7, SymbolType.Wild},
                {SymbolType.Wild, SymbolType.Sym7, SymbolType.Scatter},
                {SymbolType.Sym7, SymbolType.Scatter, SymbolType.Wild},
                {SymbolType.Scatter, SymbolType.Wild, SymbolType.Scatter}};



        Json json = new Json();
        json.addClassTag("SymbolType", SymbolType.class);
        String jsonText = json.prettyPrint(list);

        try (PrintWriter writer = new PrintWriter(new File("config/reelsetConfig"))) {

            writer.write(jsonText);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }*/
    }
}
