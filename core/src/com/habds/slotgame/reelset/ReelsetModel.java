package com.habds.slotgame.reelset;

import com.habds.slotgame.symbol.SymbolType;

import java.util.ArrayList;
import java.util.List;

public class ReelsetModel {
    SymbolType[][] reelset;
    boolean isFullPackOfSymbols;  // це повний набір символів для ріла, чи тілкьи 3 ?

    public ReelsetModel() {
    }

    public ReelsetModel(SymbolType[][] reelset) {
        this.reelset = reelset;
    }

    public ReelsetModel(List<List<SymbolType>> _reelset) {
        for (int i = 0; i < _reelset.size(); i++)
            for (int j = 0; j < _reelset.get(i).size(); j++)
                reelset[i][j] = _reelset.get(i).get(j);
    }

    public void setReelset(SymbolType[][] reelset) {
        this.reelset = reelset;
    }

    public void setReelset(List<List<SymbolType>> _reelset) {
        for (int i = 0; i < _reelset.size(); i++) {
            for (int j = 0; j < _reelset.get(i).size(); j++) {
                reelset[i][j] = _reelset.get(i).get(j);
            }
        }
    }

    public List<List<SymbolType>> getReelset() {
        List<List<SymbolType>> list = new ArrayList<>();
        for (int i = 0; i < reelset.length; i++) {
            List<SymbolType> sublist = new ArrayList<>();
            for (int j = 0; j < reelset[i].length; j++) {
                sublist.add(reelset[i][j]);
            }
            list.add(sublist);
        }
        return list;
    }
}
