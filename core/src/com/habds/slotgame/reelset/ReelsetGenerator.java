package com.habds.slotgame.reelset;

import com.habds.slotgame.reels.SpinningType;
import com.habds.slotgame.symbol.SymbolType;
import com.habds.slotgame.utils.IReelsetGenerator;

import java.util.Arrays;
import java.util.Random;

public class ReelsetGenerator implements IReelsetGenerator {

    int reelsCount=5;

    public ReelsetGenerator() {
    }


    private ReelsetModel generateReelsetModel() {
        Random random = new Random();
        SymbolType[][] symbols=new SymbolType[reelsCount][3];
        for (int i = 0; i < reelsCount; i++) {
            int type=random.nextInt(SpinningType.values().length-1);
            SymbolType symbolType=SymbolType.values()[type];
            SymbolType[] reel={symbolType,symbolType,symbolType};
            symbols[i]= reel;
        }
        return new ReelsetModel(symbols);
    }


    @Override
    public Result generateResult() {
        Result result=new Result(Arrays.asList(1,3,5,7),1500,generateReelsetModel());

        return result;
    }


}
