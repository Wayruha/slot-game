package com.habds.slotgame.reelset;

public class ReelsetConfig {
    int reelWidth = 100;
    int reelHeight = 300;
    final double hSpacingBtwSymbols;
    final double vSpacingBtwSymbols;
    final int symWidth;               //розміри символа
    final int symHeight;
    int symbolsCountInReel = 3;
    int spacingBetweenReels = 30;
    double[] reelsSpinTime = {3, 3.4, 3.8, 4.3, 4.8};

    public ReelsetConfig() {
        symWidth = (int) (0.9 * reelWidth);
        symHeight = (int) (0.33 * reelHeight);   //33% якщо немає проміжків
        hSpacingBtwSymbols = (int) (0.05 * reelWidth);
        vSpacingBtwSymbols = (int) (0.04 * reelHeight);
    }

    public int getReelWidth() {
        return reelWidth;
    }

    public int getReelHeight() {
        return reelHeight;
    }

    public int getSymWidth() {
        return symWidth;
    }

    public int getSymHeight() {
        return symHeight;
    }

    public double getVerticalSpacingBtwSymbols() {
        return vSpacingBtwSymbols;
    }

    public double getHorizontalSpacingBtwSymbols() {
        return hSpacingBtwSymbols;
    }

    public int getSymbolsCountInReel() {
        return symbolsCountInReel;
    }

    public int getSpacingBetweenReels() {
        return spacingBetweenReels;
    }

    public double[] getReelsSpinTime() {
        return reelsSpinTime;
    }

    public void setReelsSpinTime(double[] reelsSpinTime) {
        this.reelsSpinTime = reelsSpinTime;
    }
}
