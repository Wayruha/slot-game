package com.habds.slotgame.reelset;

import com.badlogic.gdx.utils.Json;
import com.habds.slotgame.symbol.SymbolType;
import com.habds.slotgame.utils.IReelsetGenerator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ReelsetLoader implements IReelsetGenerator {

    String filePath = "config/reelsetConfig";
    String jsonText;

    @Override
    public Result generateResult() {
        Result result = new Result(Arrays.asList(), 0, generateReelsetModel());

        return result;
    }

    private ReelsetModel generateReelsetModel() {
        readJsonFromFile();
        Json json = new Json();
        SymbolType[][] reelsetModel = json.fromJson(SymbolType[][].class, jsonText);
        return new ReelsetModel(reelsetModel);
    }

    private void readJsonFromFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = reader.readLine();
            }
            jsonText = sb.toString();
        } catch (IOException e) {
            System.out.println("Can`t read reelsetConfig");
            e.printStackTrace();
        }


    }
}
