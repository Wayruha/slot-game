package com.habds.slotgame.elements;

import aurelienribon.tweenengine.Tween;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.habds.slotgame.screens.SlotScreen;
import com.habds.slotgame.tweenEngine.CountUpAccesor;

public class CountUp extends Label {
    int coins;
    float duration = 1f;

    public CountUp(CharSequence text, Skin skin) {
        super(text, skin);
        Tween.registerAccessor(CountUp.class, new CountUpAccesor());
    }

    public void reset() {
        setCoins(0);
    }

    public void showNewValue(int value) {
        Tween.to(this, CountUpAccesor.COINS, duration)
                .target(value)
                .start(SlotScreen.getTweenManager());
    }

    public void setCoins(int coins) {
        this.coins = coins;
        setText(String.valueOf(coins));
    }

    public int getCoins() {
        return coins;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }
}
