package com.habds.slotgame.symbol;

public enum SymbolType {
    Wild("Wild.jpg"),
    Scatter("Scatter.jpg"),
    cherry("cherry.png"),
    diamond("diamond.png"),
    crown("crown.png"),
    poker("poker.png"),
    watermelon("watermelon.png"),
    Sym3,
    Sym4,
    Sym5,
    Sym6,
    Sym7("Sym7.jpg"),
    Sym8,
    Sym9,
    Sym10;

    private String fileName;

    SymbolType(String fileName) {
        this.fileName = fileName;
    }

    SymbolType() {
    }

    public String getFileName() {
        return fileName;
    }
}
