package com.habds.slotgame.symbol;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.habds.slotgame.reels.Container;
import com.habds.slotgame.reels.Reel;
import com.habds.slotgame.utils.Renderable;

public class Symbol extends Sprite implements Renderable {       ////TODO чи нада його імплементувати?
    SymbolType type;
    Reel reel;
    int offsetX;
    int offsetY;

    public void draw(Batch batch, int i) {  //TODO
        double k = (double) getRegionHeight() / reel.getSymHeight(); // відношення текселів до пікслеів
        if (i == 0)
            batch.draw(getTexture(),              //висота з якої починає малюватис спрайт
                    getX(),
                    getY(),   //------
                    reel.getSymWidth() / 2,
                    reel.getSymHeight() / 2,
                    reel.getSymWidth(),
                    reel.getSymHeight() - reel.getSrcY(),   //------              //якої висоти робити спрайт
                    getScaleX(),
                    getScaleY(),
                    0,
                    getRegionX(),
                    (int) (reel.getSrcY() * k),           //------      //початкова позиція на текстурі для малювання
                    getRegionWidth(),
                    (int) (getRegionHeight() - reel.getSrcY() * k), //------      //скільки текстури малювати
                    false,
                    false);
        else if (i == reel.getVisibleSymbols().size() - 1) {
            batch.draw(getTexture(),
                    getX(),
                    getY() + (reel.getSymHeight() - reel.getSrcY()),
                    reel.getSymWidth() / 2,
                    reel.getSymHeight() / 2,
                    reel.getSymWidth(),
                    reel.getSrcY(),
                    getScaleX(),
                    getScaleY(),
                    0,
                    getRegionX(),
                    getRegionY(),
                    getRegionWidth(),
                    (int) (reel.getSrcY() * k),
                    false,
                    false);
        } else {
            draw(batch);
        }
    }

    public void update() {
        this.setX(offsetX + reel.getX());
        this.setY(offsetY + reel.getY());
    }

    public Symbol(SymbolType type, Reel reel) {
        super(new Texture(Gdx.files.internal("symbols/" + type.getFileName())));
        this.type = type;
        this.reel = reel;

    }

    public Container getContainer() {
        return reel;
    }

    public void setReel(Reel reel) {
        this.reel = reel;
    }

    public SymbolType getType() {
        return type;
    }

    public void setType(SymbolType name) {
        this.type = type;
    }

    public void setOffsetY(int offsetY) {
        this.offsetY = offsetY;
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public int getXRelativeToReelset() {
        return offsetX + reel.getOffsetX();
    }

    public int getYRelativeToReelset() {
        return offsetY + reel.getOffsetY();
    }

    @Override
    public String toString() {
        return type.toString();
    }
}
