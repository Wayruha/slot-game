package com.habds.slotgame;

import com.badlogic.gdx.Game;
import com.habds.slotgame.screens.SlotScreen;

public class SlotGame extends Game {
    private static SlotGame instance;

    public static SlotGame getInstance() {
        return instance;
    }

    public void create() {
        instance = this;
        setScreen(new SlotScreen());
    }
}
