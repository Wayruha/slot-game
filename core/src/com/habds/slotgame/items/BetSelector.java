package com.habds.slotgame.items;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class BetSelector extends Selector {
    private Label valueLabel;
    private Button prevButt, nextButt;
    private Skin skin;

    public BetSelector(Skin skin) {
        this.skin = skin;
        createElements();
        layoutElements();
    }

    private void createElements() {
        ImageButton.ImageButtonStyle stylePrev = new ImageButton.ImageButtonStyle();
        stylePrev.up = skin.getDrawable("tree-minus");
        stylePrev.down = skin.getDrawable("tree-minus");

        ImageButton.ImageButtonStyle styleNext = new ImageButton.ImageButtonStyle();
        styleNext.up = skin.getDrawable("tree-plus");
        styleNext.down = skin.getDrawable("tree-plus");

        prevButt = new ImageButton(stylePrev);
        nextButt = new ImageButton(styleNext);
        prevButt.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.out.println("nextButt clicked");
                prevValue();
                refreshLabel();
            }
        });

        nextButt.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                System.out.println("nextButt clicked");
                nextValue();
                refreshLabel();
            }
        });

        valueLabel = new Label(String.valueOf(getIntValue()), skin);
    }

    private void layoutElements() {
        add(prevButt);
        add(valueLabel);
        add(nextButt);
    }

    private void refreshLabel() {
        valueLabel.setText(String.valueOf(getIntValue()));
    }
}
