package com.habds.slotgame.items;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

public abstract class Selector extends Table {
    private double[] values;
    private int valueIndex;

    public Selector() {
        valueIndex = 0;
        values = new double[]{10, 25, 50, 100, 250, 500, 1000, 1500};
    }

    public double nextValue() {
        if (values.length > 0) {
            valueIndex = (valueIndex + 1) % values.length;
        }
        return values[valueIndex];
    }

    public double prevValue() {
        if (values.length > 0) {
            valueIndex = (valueIndex - 1 + values.length) % values.length;
        }
        return values[valueIndex];
    }

    public double getValue() {
        return values[valueIndex];
    }

    public int getIntValue() {
        return (int) values[valueIndex];
    }
}
